uses crt;

var
x,y,keys:word;
tab:array[1..9]of char;
last,key,lst:char;
wygr,bolnew:boolean;
ix,io,runda:byte;
snd,mp:char;

procedure ktomaruch;
begin
 textcolor(white);
 gotoxy(16,12);
 if (last='O') then begin write('Ruch ma: '); textcolor(lightred); write('X'); end
 else begin write('Ruch ma: '); textcolor(yellow); write('O'); end
end;

function dzwiek:boolean;
begin
 if snd='t' then dzwiek:=true else dzwiek:=false;
end;

procedure beep(a,b:word);
begin
 if dzwiek then
 begin
  {refresh;}
  sound(a);
  delay(b);
  nosound;
 end;
end;

procedure msp;
begin
asm
  mov ax, 1;
  int 33h;
end;

end;

procedure opcje;
begin
 textcolor(white);
 gotoxy(16,5);
 write('m - multiplayer [',mp,']');
 gotoxy(16,6);
 write('s - dzwiek [',snd,']');
end;

procedure punkty;
begin
 gotoxy(16,2);
 textcolor(lightred); write('X'); textcolor(white); write(' - ',ix);
 gotoxy(16,3);
 textcolor(yellow); write('O'); textcolor(white); write(' - ',io);
 gotoxy(16,8);
 textcolor(white); write('Runda: ',runda)
end;

procedure wygrana(l:char);
begin
 textcolor(white);
 wygr:=true;
 gotoxy(3,14);
 if l='O' then begin write('Wygralo: '); textcolor(yellow); write(l); io:=io+1;  end;
 if l='X' then begin write('Wygral: '); textcolor(lightred); write(l); ix:=ix+1; end;
 punkty;
 beep(500,300);
 beep(300,200);
end;

procedure rysujgre2;

const
 kr=lightred;
 ko=yellow;

var
 ch:char;

begin
 gotoxy(3,3); ch:=tab[1]; if ch='X' then textcolor(kr) else textcolor(ko); write(ch);
 gotoxy(7,3); ch:=tab[2]; if ch='X' then textcolor(kr) else textcolor(ko); write(ch);
 gotoxy(11,3); ch:=tab[3]; if ch='X' then textcolor(kr) else textcolor(ko); write(ch);
 gotoxy(3,7); ch:=tab[4]; if ch='X' then textcolor(kr) else textcolor(ko); write(ch);
 gotoxy(7,7); ch:=tab[5]; if ch='X' then textcolor(kr) else textcolor(ko); write(ch);
 gotoxy(11,7); ch:=tab[6]; if ch='X' then textcolor(kr) else textcolor(ko); write(ch);
 gotoxy(3,11); ch:=tab[7]; if ch='X' then textcolor(kr) else textcolor(ko); write(ch);
 gotoxy(7,11); ch:=tab[8]; if ch='X' then textcolor(kr) else textcolor(ko); write(ch);
 gotoxy(11,11); ch:=tab[9]; if ch='X' then textcolor(kr) else textcolor(ko); write(ch);

 if
  (
  ((tab[1]='O')and(tab[2]='O')and(tab[3]='O'))or
  ((tab[4]='O')and(tab[5]='O')and(tab[6]='O'))or
  ((tab[7]='O')and(tab[8]='O')and(tab[9]='O'))or

  ((tab[1]='O')and(tab[5]='O')and(tab[9]='O'))or
  ((tab[3]='O')and(tab[5]='O')and(tab[7]='O'))or

  ((tab[1]='O')and(tab[4]='O')and(tab[7]='O'))or
  ((tab[2]='O')and(tab[5]='O')and(tab[8]='O'))or
  ((tab[3]='O')and(tab[6]='O')and(tab[9]='O'))
  )or
  (
  ((tab[1]='X')and(tab[2]='X')and(tab[3]='X'))or
  ((tab[4]='X')and(tab[5]='X')and(tab[6]='X'))or
  ((tab[7]='X')and(tab[8]='X')and(tab[9]='X'))or

  ((tab[1]='X')and(tab[5]='X')and(tab[9]='X'))or
  ((tab[3]='X')and(tab[5]='X')and(tab[7]='X'))or

  ((tab[1]='X')and(tab[4]='X')and(tab[7]='X'))or
  ((tab[2]='X')and(tab[5]='X')and(tab[8]='X'))or
  ((tab[3]='X')and(tab[6]='X')and(tab[9]='X'))
  )

 then wygrana(last);
 
 ktomaruch;
 
end;

Procedure put(w:byte);
begin

if last='X' then
 begin
  tab[w]:='O';
  last:='O';
  beep(600,200);
 end else if last='O' then
  begin
   tab[w]:='X';
   last:='X';
   beep(700,200);
  end;
 rysujgre2;
 bolnew:=true; 
end;

procedure mz(t,u,w:byte);
var z,r:byte;
begin

if t=1 then z:=0;
if t=2 then z:=4;
if t=3 then z:=8;

if u=1 then r:=0;
if u=2 then r:=4;
if u=3 then r:=8;

if ((x=2+z)and(y=2+r))or
((x=3+z)and(y=2+r))or
((x=4+z)and(y=2+r))or
((x=2+z)and(y=3+r))or
((x=3+z)and(y=3+r))or
((x=4+z)and(y=3+r))or
((x=2+z)and(y=4+r))or
((x=3+z)and(y=4+r))or
((x=4+z)and(y=4+r)) then if (tab[w]=' ')and(wygr=false) then put(w);
end;

procedure mzk(w:byte);
begin
 if (tab[w]=' ')and(wygr=false) then put(w);
end;

procedure mpos;
begin
asm
  mov ax, 3;
  int 33h;
  mov x, cx;
  mov y, dx;
  mov keys, bx;
end;

x:=x div 8+1;
y:=y div 8+1;

end;

procedure rysujgre;
var y:byte;
begin
 clrscr;

 textcolor(white);

 punkty;

 textcolor(green);

 for y:=2 to 12 do
 begin
  gotoxy(5,y);
  write(chr(179));
  gotoxy(9,y);
  write(chr(179));

  gotoxy(y,5);
  write(chr(196));
  gotoxy(y,9);
  write(chr(196));
 end;

 gotoxy(5,5);
 write(chr(197));

 gotoxy(9,5);
 write(chr(197));

 gotoxy(5,9);
 write(chr(197));

 gotoxy(9,9);
 write(chr(197));

 msp;

end;

procedure graj;
var
rnd:byte;
label gr;
begin

gr:

if mp='t' then
 repeat
 mpos;
  if keys=1 then
   begin      
    mz(1,1,1);
    mz(2,1,2);
    mz(3,1,3);
    mz(1,2,4);
    mz(2,2,5);
    mz(3,2,6);
    mz(1,3,7);
    mz(2,3,8);
    mz(3,3,9);
    keys:=0;
   end;
 until keypressed;

if mp='n' then
 repeat
  mpos;
  if keys=1 then
   begin
    if last='O' then
     begin
	  mz(1,1,1);
      mz(2,1,2);
      mz(3,1,3);
      mz(1,2,4);
      mz(2,2,5);
      mz(3,2,6);
      mz(1,3,7);
      mz(2,3,8);
      mz(3,3,9);
      keys:=0;	  
     end;
    end else if last='X' then begin
     repeat
      rnd:=random(10);
     until (rnd>0)and(rnd<10);
	 {refresh;}
	 delay(100);
     mzk(rnd);
    end;
 until keypressed;

 key:=readkey;
 if key=#27 then begin textcolor(lightgray); clrscr; halt; end;
 if key='s' then if snd='n' then snd:='t' else snd:='n';
 if key='m' then if mp='n' then mp:='t' else mp:='n';

 if key='s' then begin opcje; goto gr; end;
 if key='m' then begin last:='O'; ix:=0; io:=0; runda:=1; bolnew:=false; end;
end;

begin
 last:='O';
 snd:='n';
 mp:='t';
 runda:=1;
 bolnew:=false;
 repeat
  wygr:=false;
  for x:=1 to 9 do tab[x]:=' ';
  rysujgre;
  opcje;
  ktomaruch;
  graj;
  if bolnew then runda:=runda+1;
  bolnew:=false;
 until key=#27;
end.
